#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>

using namespace std;

class AbsVisitor : public boost::static_visitor<double>
{
public:
    template <typename T>
    double operator()(const T&  value) const
    {
        return abs(value);
    }

    double operator()(float value) const
    {
        return fabs(value);
    }

//    double operator()(const complex<double>& value)
//    {
//        return abs(value);
//    }
};

int main()
{
    typedef boost::variant<int, float, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));

    // TODO: korzystając z mechanizmu wizytacji wypisać na ekranie moduły liczb
    AbsVisitor abs_visitor;
    transform(vars.begin(), vars.end(),
              ostream_iterator<double>(cout, " "),
              boost::apply_visitor(abs_visitor));

    cout << "\n";
}
