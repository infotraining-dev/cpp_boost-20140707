#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

template <typename T>
void print_tuple(T& t, string prefix)
{
	cout << boost::tuples::set_open('(')
		 << boost::tuples::set_close(')')
		 << boost::tuples::set_delimiter(',')
		 << prefix << " = " << t << endl;
}


template <typename Container>
boost::tuple<typename Container::value_type, typename Container::value_type>
    find_min_max(const Container& c)
{
    auto min_it = min_element(begin(c), end(c));
    auto max_it = max_element(begin(c), end(c));

    return boost::make_tuple(*min_it, *max_it);
}

template <typename Tuple, typename Func, size_t Index>
struct ForEachHelper
{
    static void for_each(Tuple& t, Func f)
    {
        ForEachHelper<Tuple, Func, Index - 1>::for_each(t, f);
        f(boost::tuples::get<Index>(t));
    }
};

template <typename Tuple, typename Func>
struct ForEachHelper<Tuple, Func, 0>
{
    static void for_each(Tuple& t, Func f)
    {
        f(boost::tuples::get<0>(t));
    }
};

template <typename Tuple, typename Func>
void tuple_for_each(Tuple& t, Func f)
{
    ForEachHelper<Tuple, Func, boost::tuples::length<Tuple>::value-1>::for_each(t, f);
}

template <typename Tuple, typename Func>
void simpler_for_each(Tuple& t, Func f)
{
    f(t.get_head());

    simpler_for_each(t.get_tail(), f);
}

template <typename Func>
void simpler_for_each(boost::tuples::null_type, Func f)
{}

class Printer
{
public:
    void operator()(int x) const
    {
        cout << "int: " << x << endl;
    }

    void operator()(double d) const
    {
        cout << "double: " << d << endl;
    }

    void operator()(const string& s) const
    {
        cout << "string: " << s << endl;
    }
};

int main()
{
    typedef boost::tuple<int, double, string> Tuple;

    cout << "Lenght of Tuple: " << boost::tuples::length<Tuple>::value << endl;

    // krotka
	boost::tuple<int, double, string> triple(43, 3.1415, "Krotka...");

    simpler_for_each(triple, Printer());

    cout << "krotka: " << triple << endl;

	// odwolania do krotki
	cout << "triple = ("
		 << triple.get<0>() << ", "
		 << triple.get<1>() << ", "
		 << boost::tuples::get<2>(triple) << ")" << endl;


    // domyslna inicjalizacja krotki
	boost::tuple<short, bool, string> default_tuple;
	print_tuple(default_tuple, "default_tuple");

	// funkcja pomocnicza - make tuple
	triple = boost::make_tuple(12, 32.222, "Inna krotka...");
	print_tuple(triple, "triple");

	triple.get<2>() = "Inny tekst...";
	print_tuple(triple, "triple");

	// krotki z referencjami
	int x = 10;
	string str = "Tekst...";

	boost::tuple<int&, string&> ref_tpl(x, str);
	ref_tpl.get<0>()++;
	boost::to_upper(ref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    boost::tuple<const int&, string&> cref_tpl
            = boost::make_tuple(boost::cref(x), boost::ref(str));

	//cref_tpl.get<0>()++; // Blad! Referencja do stalej!
	boost::to_lower(cref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    boost::tuple<int, string> t2;

    t2.get<0>() = 10;
    t2.get<1>() = "Tekst";

    t2.get<0>()++;

    cout << "t2: " << t2 << endl;

    int min;
    //int max;

    vector<int> vec = { 5, 1, 9, 8, 6, 3, 99, 2, 10 };

    //boost::tuple<int&, int&> result(min, max);

    boost::tie(min, boost::tuples::ignore) = find_min_max(vec);

    cout << "min: " << min << endl;
    //cout << "max: " << max << endl;
}
