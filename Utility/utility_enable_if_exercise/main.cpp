#include <iostream>
#include <iterator>
#include <list>
#include <boost/type_traits/is_pod.hpp>
#include <boost/utility/enable_if.hpp>
#include <cstring>

using namespace std;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres
//     elementow typu T [first, last) do kontenera rozpoczynającego się od dest
template <typename InIter, typename OutIter>
OutIter mcopy(InIter start, InIter end, OutIter dest)
{
    cout << "Generic mcopy" << endl;

    while(start != end)
    {
        *(dest++) = *(start++);
    }

    return dest;
}

template <typename T>
typename boost::enable_if<boost::is_pod<T>, T*>::type
    mcopy(T* start, T* end, T* dest)
{
    cout << "Optimized mcopy" << endl;

    size_t size_of_array = end - start;
    memcpy(dest, start, size_of_array * sizeof(T));

    return start + size_of_array;
}


// 2 - napisz zoptymalizowaną wersję mcopy
//     wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
// TODO

template <typename T, typename Enable = void>
class DataProcessor
{

};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_float<T> >::type>
{
};


void ub_optimization()
{
    int i = 1;

    while(i > 0)
    {
        ++i;
    }
}

int main()
{
    string words[] = { "one", "two", "three", "four" };

    list<string> list_of_words(4);

    mcopy(words, words + 4, list_of_words.begin()); // działa wersja generyczna

    mcopy(list_of_words.begin(), list_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers[] = { 1, 2, 3, 4, 5 };
    int target[5];

    mcopy(numbers, numbers + 5, target); // działa wersja zoptymalizowana

    mcopy(target, target + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    DataProcessor<int> dp1;

    DataProcessor<float> dp2;

    ub_optimization();
}

