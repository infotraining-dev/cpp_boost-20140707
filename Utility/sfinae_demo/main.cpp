#include <iostream>
#include <boost/type_traits/is_integral.hpp>
#include <boost/utility/enable_if.hpp>

using namespace std;

int foo(int arg)
{
    cout << "foo(int: " << arg << ")" << endl;

    return arg;
}


template <typename T>
typename boost::disable_if<boost::is_integral<T>, T>::type
 foo(T arg)
{
    cout << "foo<T>(T: " << arg << ")" << endl;

    return arg;
}

//template <typename T>
//typename T::nested_type foo(T arg)
//{
//    cout << "foo<T>(T: " << arg << ")" << endl;
//}

//struct BigInt
//{
//    int value;

//    typedef void nested_type;
//};

//ostream& operator<<(ostream& out, const BigInt& bi)
//{
//    out << bi.value;

//    return out;
//}

int main()
{
    foo(8);

    int x = 10;
    foo(x);

    short sx = 5;
    foo(sx);

//    BigInt v { 10 };

//    foo(v);

    double d = 3.14;

    cout << foo(d) << endl;
}

