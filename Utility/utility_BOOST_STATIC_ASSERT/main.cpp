#include <iostream>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <memory>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_255(T (&arr)[i])
{
    BOOST_STATIC_ASSERT(i>=1 && i<=255);
}

void expects_ints_to_be_4_bytes()
{
    BOOST_STATIC_ASSERT(sizeof(int) == 4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}


template <typename T>
struct is_auto_ptr : public boost::false_type
{};

template <typename T>
struct is_auto_ptr<std::auto_ptr<T>> : public boost::true_type
{};

template <typename T>
void works_with_pointers(T ptr)
{
    BOOST_STATIC_ASSERT(is_auto_ptr<T>::value == false
                        && "auto_ptr<T> nie jest dozwolonym parametrem");

//    static_assert(is_auto_ptr<T>::value == false,
//                  "auto_ptr<T> nie jest dozwolonym parametrem");

    std::cout << "Value: " << *ptr << std::endl;
}

int main()
{
    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[250];

    accepts_arrays_with_size_between_1_and_255(arr);

	Derived arg;
	works_with_base_and_derived(arg);

    int* ptr1 = new int(15);

    works_with_pointers(ptr1);

    delete ptr1;

    std::shared_ptr<int> ptr2(new int(16));

    works_with_pointers(ptr2);

    std::auto_ptr<int> ptr3(new int(17));

    works_with_pointers(ptr3);
}
