#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <algorithm>
#include <boost/checked_delete.hpp>
#include <boost/utility/addressof.hpp>

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);

    Deleter* p = boost::addressof(exterminator);

    std::cout << p << std::endl;
}

int main()
{
    using namespace std;

    {
        vector<ToBeDeleted*> vec
                = { new ToBeDeleted(), new ToBeDeleted(), new ToBeDeleted() };

        for_each(vec.begin(), vec.end(), &boost::checked_delete<ToBeDeleted>);
    }


	real_test();
}
