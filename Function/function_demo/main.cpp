#include <iostream>
#include <boost/function.hpp>
#include <boost/bind.hpp>

using namespace std;

class Service
{
    boost::function<void (string)> on_run_callback;
    boost::function<void ()> on_close_callback;
public:
    void run(const string& arg)
    {
        cout << "Service::run()" << endl;

        if (on_run_callback)
            on_run_callback(arg);
    }

    void close()
    {
        cout << "Service::close()" << endl;

        if (on_close_callback)
            on_close_callback();
    }

    void set_on_run_callback(boost::function<void (string)> callback)
    {
        on_run_callback = callback;
    }

    void set_on_close_callback(boost::function<void ()> callback)
    {
        on_close_callback = callback;
    }
};

void log(const string& message)
{
    cout << "log: " << message << endl;
}

class Db
{
public:
    void save_to_db(const string& msg)
    {
        cout << "Db: " << msg << endl;
    }
};

int main()
{
    Db database;

    Service srv;
    srv.set_on_run_callback(&log);
    srv.set_on_close_callback(boost::bind(&Db::save_to_db, &database, "Closing service"));

    srv.run("Test1");

    srv.close();
}

