#include <iostream>
#include <boost/cast.hpp>

using namespace std;

int main() try
{
    int i = 6700;

    short s = boost::numeric_cast<short>(i);

    cout << "i = " << i << endl;
    cout << "s = " << s << endl;

    char c = 5;

    unsigned char uc = boost::numeric_cast<unsigned char>(c);

    cout << "c = " << static_cast<int>(c) << endl;
    cout << "uc = " << static_cast<int>(uc) << endl;

    return 0;
}
catch(const boost::bad_numeric_cast& e)
{
    cout << "Exception: " << e.what() << endl;
}

