#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>

using namespace std;

class X
{
public:
    // konstruktor
    X(int value = 0)
        : value_(value)
    {
        std::cout << "Konstruktor X(" << value_ << ")\n";
    }

    // destruktor
    ~X()
    {
        std::cout << "Destruktor ~X(" << value_ << ")\n";
    }

    int value() const
    {
        return value_;
    }

    void set_value(int value)
    {
        value_ = value;
    }

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
    int value_;
};


class A
{
	boost::shared_ptr<X> x_;
public:
	A(boost::shared_ptr<X> x) : x_(x) {}
	
	int get_value()
	{
		return x_->value();
	}
};

class B
{
	boost::shared_ptr<X> x_;
public:
	B(boost::shared_ptr<X> x) : x_(x) {}
	
	void set_value(int i)
	{
        x_->set_value(i);
	}	
};

class BaseManagedObject
{
    int id_;
public:
    BaseManagedObject(int id = 0) : id_(id)
    {
        cout << "ManagedObject(" << id_ << ")" << endl;
    }

    virtual ~BaseManagedObject()
    {
        cout << "~ManagedObject(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }
};

class Derived : public BaseManagedObject
{
    string value_;
public:
    Derived(int id, string val) : BaseManagedObject(id), value_(val)
    {
        cout << "Derived(" << id << ", " << val << ")" << endl;
    }

    ~Derived()
    {
        cout << "~Derived("  << id() << ", " << value_ << ")" << endl;
    }

    string value() const
    {
        return value_;
    }
};



int main()
{
	{
		boost::shared_ptr<X> spX1(new X());
		std::cout << "RC: " << spX1.use_count() << std::endl;
		{
			boost::shared_ptr<X> spX2(spX1);
			std::cout << "RC: " << spX2.use_count() << std::endl;
		}
		std::cout << "RC: " << spX1.use_count() << std::endl;
	}

	std::cout << "\n//----------------------\n\n";

    boost::shared_ptr<X> temp = boost::make_shared<X>(14);
	A a(temp);
	{
		B b(temp);
		b.set_value(28);
	}
	
	assert(a.get_value() == 28);
	std::cout << "a.get_value() = " << a.get_value() << std::endl;

    std::cout << "\n//----------------------\n\n";

    boost::shared_ptr<BaseManagedObject> ptr_base(new Derived(13, "trzynascie"));

    ptr_base->id();

    boost::shared_ptr<Derived> ptr_derived = boost::static_pointer_cast<Derived>(ptr_base);

    cout << ptr_derived->value() << endl;
}
