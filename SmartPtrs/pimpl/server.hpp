#ifndef SERVER_HPP
#define SERVER_HPP

#include <string>
#include <boost/scoped_ptr.hpp>
#include <memory>

class Server
{
    class Impl;
    std::unique_ptr<Impl> impl_;
public:
    Server();
    ~Server();

    Server(Server&& src);
    Server& operator=(Server&& src);
    void send(const std::string& message);
};

#endif
