#include <iostream>
#include <memory>
#include <cassert>
#include <vector>

using namespace std;

class Object
{
    int value_;
public:
    Object(int v = -1) : value_(v)
    {
        cout << "Object(" << value_ << ")" << endl;
    }

    ~Object()
    {
        cout << "~Object(" << value_ << ")" << endl;
    }

    int value() const
    {
        return value_;
    }

    void set_value(int v)
    {
        value_ = v;
    }
};

auto_ptr<Object> factory(int v)
{
    return auto_ptr<Object>(new Object(v));
}

void foo(auto_ptr<Object> ptr)
{
    cout << "foo: " << ptr->value() << endl;
}

unique_ptr<Object> unique_factory(int v)
{
    unique_ptr<Object> result(new Object(v));

    return result;
}

void unique_foo(unique_ptr<Object> ptr)
{
    cout << "unique_foo: " << ptr->value() << endl;
}

int main()
{
    {
        auto_ptr<Object> aptr1 = factory(1);

        //foo(aptr1);

        cout << (*aptr1).value() << endl;
        cout << aptr1->value() << endl;

        aptr1.reset(new Object(2));



        auto_ptr<Object> aptr2 = aptr1;  // transfer prawa wlasnosci
        cout << aptr2->value() << endl;

        assert(aptr1.get() == NULL);

        cout << "END OF SCOPE" << endl;
    }

    cout << "\n-------------------------\n";

    {
        unique_ptr<Object> aptr1(new Object(1));

        unique_ptr<Object> aptr2 = move(aptr1);

        unique_ptr<Object> aptr3 = unique_factory(8);

        unique_foo(unique_factory(6));

        vector<unique_ptr<Object>> vec;

        vec.push_back(move(aptr3));
        vec.push_back(unique_factory(13)); // push_back(T&&)
        vec.emplace_back(new Object(33));

        unique_ptr<Object[]> arr(new Object[10]);

        arr[3].set_value(10);
    }
}

