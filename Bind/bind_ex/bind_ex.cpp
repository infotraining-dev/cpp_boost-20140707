#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <numeric>
#include <boost/bind.hpp>
#include <boost/function.hpp>

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::salary, _1) > 3000.0);


    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";

    auto func = boost::bind(&Person::age, _1) < 30;

    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            func);

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
    sort(employees.begin(), employees.end(),
         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
            boost::bind(&Person::gender, _1) == Female);

    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";

//    double avg =
//            accumulate(employees.begin(), employees.end(),
//                       0.0,
//                       boost::bind(plus<double>(), _1,
//                                   boost::bind(&Person::salary, _2))) / employees.size();

    double avg =
            accumulate(employees.begin(), employees.end(),
                       0.0,
                       [](double d, const Person& p) { return d + p.salary(); }) / employees.size();

    cout << count_if(employees.begin(), employees.end(),
                     boost::bind(&Person::salary, _1) > avg);

    cout << "\nAvg: " << avg << endl;



}
