#include <iostream>
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>

using namespace std;

void func2(int x, double y)
{
    cout << "func2(" << x << ",  " << y << ")" << endl;
}

string func3(const string& text, int value, int& counter)
{
    counter++;

    return text + boost::lexical_cast<string>(value);
}

class Add //: public std::binary_function<int, int, int>
{
public:
    //typedef int result_type;

    int operator()(int a, int b) const
    {
        return a + b;
    }
};

class BoundFunc
{
    int value_;
public:
    BoundFunc(int value) : value_(value)
    {}

    void operator()(double deffered)
    {
        func2(value_, deffered);
    }
};

class Person
{
    int id_;
    string name_;
    int age_;
public:
    Person(int id, const string& name, int age) : id_(id), name_(name), age_(age)
    {}

    int age() const
    {
        return age_;
    }

    void print(const string& prefix) const
    {
        cout << prefix << " " << id_ << " - " << name_
             << " - age: " << age_ << endl;
    }

    bool is_retired() const
    {
        return age_ > 67;
    }

    bool is_retired(int threshold) const
    {
        return age_ > threshold;
    }
};

int main()
{
    auto f1 = boost::bind(&func2, 2, _1);

    f1(3.14);  // func2(2, 3.14)
    f1(7.23);

    auto f0 = boost::bind(&func2, 5, 7.88);

    f0(); // func2(5, 7.88)
    f0();

    int count = 0;
    string text = "Test: ";

    auto f2 = boost::bind(&func3, boost::cref(text), _1, boost::ref(count));

    string result = f2(1);

    cout << "result: " << result << "; count: " << count << endl;

    auto f3 = boost::bind(&func3, boost::cref(text), _1, _2);

    result = f3(2, count);

    cout << "result: " << result << "; count: " << count << endl;

    auto f4 = boost::bind<int>(Add(), _1, 5);

    cout << "Add: " << f4(13) << endl;


    Person p1 { 1, "Kowalski", 44 };

    auto f5 = boost::bind(&Person::print, &p1, "Dane osoby: ");

    f5();

    auto f6 = boost::bind(&Person::print, _1, "Dane osoby: ");

    f6(p1);

    cout << "\n";

    vector<Person> people = { p1, Person {2, "Nowak", 78}, Person {3, "Anonim", 44 } };

    for_each(people.begin(), people.end(),
             boost::bind(&Person::print, _1, "Dane osoby: "));


    vector<Person> retired;

    copy_if(people.begin(), people.end(), back_inserter(retired),
            boost::bind(&Person::is_retired, _1, 40));

    copy_if(people.begin(), people.end(), back_inserter(retired),
            [](const Person& p) { return p.is_retired(40); });

    for_each(retired.begin(), retired.end(),
             boost::bind(&Person::print, _1, "Dane osoby na emeryturze: "));

//    auto first_retired =
//            find_if(people.begin(), people.end(),
//                    boost::bind(greater<int>(),
//                                    std::bind(&Person::age, std::placeholders::_1),
//                                    67));

    auto first_retired =
            find_if(people.begin(), people.end(),
                    boost::bind(&Person::age, _1) > 67);

    if (first_retired != people.end())
        first_retired->print("Pierwszy emeryt: ");
}

